#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Boucle WHILE comptant de 1 � 10
    unsigned int i = 1;

    printf("*** Boucle WHILE comptent de 1 a 10 : ***\n");
    while ( i <= 10)
    {
        printf("%u\n",i);
        i++;
    }

    //Boucle FOR comptant de 1 � 10
    printf("\n*** Boucle FOR comptent de 1 a 10 : ***\n");
    for (int i = 1; i <= 10; i++)
    {
        printf("%u\n",i);
    }
    return 0;
}


/*
IF ... ELSE IF ... ELSE

if (variable = X)
{
    si vrai on fait quelque chose
}
else if (variable = Y)
{
    sinon si on fait autre chose
}
else
{
    alors on fait ca
}

Switch :

switch (variable)
{
case 1: Premier cas
    Si la variable est � �gale a 1 alors on fait quelques chose
    break; permet de sortir du switch
case 2: Deuxieme cas
    Si la variable est � �gale a 2 alors on fait quelques chose
    break;permet de sortir du switch
default: Cas par default qui est obsionelle
    // si aucune condition n'est vraie, le code par d�faut sera ex�cut�
    // le cas default est optionnel (non -obligatoire)
}

Difference entre define et constatante

Un #define est d�clar� apr�s les #INCLUDE. Il est d�clar�  : #define MONDEFINE 8
Le compilateur va chercher dans le programme l'expression MONDEFINE et le remplacer par la valeur 8
Une constante est d�clar�e comme const int maConst = 10; elle ne pourra pas etre modifi�e,
elle pourra uniquement etre lue.
*/

