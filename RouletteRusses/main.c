#include <stdio.h>
#include <stdlib.h>
#include <time.h> //Ajout de la librerairie pour le random


//Proc�dure pour afficher le contenu du barillet
void afficheBarillet(int barillet[06])
{
    int i;
    for (i=0; i<6; ++i)
    {
        printf("[%d] : %d\n", i, barillet[i]);
    }
}

//Fonction cr�ation du tableau pour le barillet
int creationBarillet(int barillet[06])
{
    int i;
    for (i=0; i<6 ; ++i)
    {
        barillet[i]=0;
    }
    return barillet;
}

//Fonction al�atoire du possitionnement de la balle dans le barillet
int positionBalle(int barillet[06])
{
    return barillet[rand()%6]=1;
}

//Fonction jeu roulette
int roulette(int roulette)
{
    return 0;
}

//Fonction jeu rouletteRusse
int rouletteRusse(int rouletteRusse)
{
    return 0;
}

int main()
{
    //Initialisation du random
    srand(time(NULL));

    //D�claration des variables
    int jetonsJoueur = 10; //D�part avec 10 jetons
    int vieJoueur = 1;
    int choixCasino = 0;
    int choixJoueur;
    int miseJoueur = 0;
    int barillet[6];
    int rejouer;
    int gachette = 0;

    //Jeu de la roulette
    printf("*** Jeu de la roulette ***\n");
    printf("*** Vous avez echange votre montre contre 10 jetons ***\n");
    printf("*** Vous avez %d jetons ***\n\n", jetonsJoueur);
    printf("Veuillez choisir une couleur entre  1 = Noir (impaire) et 2 = Rouge (paire)\n");

    //Boucle de vie du joueur
    while (vieJoueur)
    {
        while(jetonsJoueur > 0)
        {
            //On replace le curseur de saisie � la fin du buffer
            fseek(stdin, 0, SEEK_END);
            scanf("%d", &choixJoueur);

            //Demande au joueur combien il veut miser
            printf("Combien voullez-vous miser ? Vous avez %d\n", jetonsJoueur);
            fseek(stdin, 0, SEEK_END);
            scanf("%d", &miseJoueur);

            //Nombre al�atoire entre 0 et 37
            choixCasino = rand()%37;

            //On test si la mise du joueur est correcte
            if ((miseJoueur <= jetonsJoueur) && (miseJoueur > 0))
            {
                //On regarde si le chiffre al�atoire est 0 si oui on perd la mise
                if (choixCasino == 0)
                {
                    printf("Le casino a mise sur le %d\n", choixCasino);
                    printf("Le casino a joue 0 vous avez perdu votre mise !!\n");
                    jetonsJoueur = jetonsJoueur - miseJoueur;
                }
                //On regarde si le chiffre est paire et le choix du joueur
                else if ((choixCasino%2) == 0 && (choixJoueur == 2))
                {
                    printf("Le casino a mise sur le %d\n", choixCasino);
                    printf("La bille est tombee sur un nombre paire\n");
                    jetonsJoueur = jetonsJoueur + miseJoueur;
                }
                //On regarde si le chiffre est impaire et le choix du joueur
                else if ((choixCasino%2) == 1 && (choixJoueur == 1))
                {
                    printf("Le casino a mise sur le %d\n", choixCasino);
                    printf("La bille est tombee sur un nombre Impaire\n");
                    jetonsJoueur = jetonsJoueur + miseJoueur;
                }
                //Dans les autres cas on perd
                else
                {
                    printf("Le casino a mise sur le %d\n", choixCasino);
                    printf("Vous avez perdu votre mise!\n");
                    jetonsJoueur = jetonsJoueur - miseJoueur;
                }
            }
            printf("La mise doit etre inferieur ou egale a %d\n", jetonsJoueur);
        }
        //Jeux de la roulette russe
        //Cr�ation du barillet
        creationBarillet(barillet);

        //On place une balle al�atoirement dans un revolver � 6 coups
        positionBalle(barillet);

        printf("\n*** Roulette Russe***\n");

        //On rentre au moins une fois dans la boucle
        do
        {
            //Si la balle est en position 0 le joueur meurt
            if (barillet[0] == 1)
            {
                printf("Tu es mort !\n");
                vieJoueur--;
                return 0;
            }
            //Sinon on demande si il veut essayer ou retourner � la roulette
            else
            {
                printf("\nVous avez eu de la chance !\n");
                printf("\nSouhaitez-vous tirer a nouveau ou passer a la roulette\n");
                printf("\n1 : Rejouer a la roulette russe \n2 : Retourner a la roulette\n", &rejouer);
                fseek(stdin, 0, SEEK_END);
                scanf("%d", &rejouer);
                barillet[gachette] = gachette+1;
            }
        }
        while(rejouer);
    }
    return 0;
}


